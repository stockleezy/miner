# Blue Mist Miner

## Setup Instructions

1. Copy `.env.example` to `.env` and open new file in your favorite text editor
2. Set the `WIF`, `MINER_UTF8`, and `CPU_COUNT` to your desired values. Optionally you can configure ZeroMQ and RPC if you have services available. Save and close file when done.
3. Open a terminal to the folder with this file in it, then run `npm i` to install dependencies
4. Run `npm start` to start the miner