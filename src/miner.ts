import * as dotenv from "dotenv";
dotenv.config();

// for multiple cpu
const cluster = require("cluster");
const minerCpuCount = parseInt(process.env.CPU_COUNT as string);
if(cluster.isMaster) { cluster.setupMaster({serialization:'advanced'}); }

// Master process
const runMaster = async () => {
	let imported = await import('./generateV1');
    let state: any = {};
    while (true) {
		cluster.removeAllListeners();
        state = await imported.generateV1(state);
    }
};

// Worker process
const runWorker = async (worker: any) => {
	let imported = await import('./workerThread');
	while(true) {
		await imported.workerThread(worker);
	}
};

if(cluster.isMaster) { runMaster(); }
else { runWorker(cluster.worker); }

